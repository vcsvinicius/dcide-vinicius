@extends('layouts.app')

@section('content')
<page size="12">
    <panel title="Carros">
        <table-list v-bind:headers="['#', 'Carro', 'Marca', 'Ano', 'Vendido']" v-bind:rows="{{ json_encode($cars) }}" urlshow="{{ url('cars/show') }}" urlcreate="{{ route('cars.create') }}" urlupdate="{{ url('cars/edit') }}" urldelete="{{ url('api/cars') }}"></table-list>
    </panel>
</page>
@endsection