@extends('layouts.app')

@section('content')
    <page size="12">
        <panel title="Editando Carro">
            <form-crud id_form="formShow" url_back="{{ route('cars.index') }}" url_back="{{ route('cars.index') }}">
                @include('cars.partials.form')
            </form-crud>
        </panel>
    </page>
@endsection