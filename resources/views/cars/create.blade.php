@extends('layouts.app')

@section('content')
    <page size="12">
        <panel title="Cadastrando Carro">
            <form-crud id_form="formCreate" method="post" action="{{ route('api.cars.store') }}" url_back="{{ route('cars.index') }}" url_success="{{ route('cars.index') }}" btn_save="true">
                @include('cars.partials.form')
            </form-crud>
        </panel>
    </page>
@endsection