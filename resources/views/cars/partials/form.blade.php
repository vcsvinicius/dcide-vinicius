<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('car', 'Carro / Model') !!}
            {!! Form::text('car', $car['car'] ?? null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('brand', 'Marca') !!}
            {!! Form::text('brand', $car['brand'] ?? null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('year', 'Ano') !!}
            {!! Form::text('year', $car['year'] ?? null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('sold', 'Vendido?') !!}
            {!! Form::select('sold', [0 => 'Não', 1 => 'Sim'], $car['sold'] ?? null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('description', 'Descrição') !!}
            {!! Form::textarea('description', $car['description'] ?? null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>