@extends('layouts.app')

@section('content')
    <page size="12">
        <panel title="Editando Carro">
            <form-crud id_form="formEdit" method="put" action="{{ route('api.cars.update', ['id_car' => $id_car]) }}" url_back="{{ route('cars.index') }}" btn_save="true">
                @include('cars.partials.form')
            </form-crud>
        </panel>
    </page>
@endsection