<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ config('app.name') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- CSS this Page -->
    @stack('css')
</head>

<body>
<div id="app">
    <top title="{{ config('app.name', 'Laravel') }}"></top>

    <br />

    @yield('content')
</div>
<script src="{{ asset('js/app.js') }}"></script>
<!-- JS this Page -->
@stack('js')
</body>