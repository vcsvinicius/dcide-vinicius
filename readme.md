# Teste Full-Stack
Teste desenvolvimento para vaga de Desenvolvedor Full-Stack para empresa Dcide - Campinas

Candidato / Programador: **Vinicius Rodrigues Pereira**

### Instalação

Após checkout do projeto seguir os seguintes passos:

- composer install
- criar o arquivo ".env", copiando o arquivo ".env.example"
- criar e configurar o acesso ao banco de dados no .env
- para criar a(s) tabela(s), acesso o console na raiz do projeto e executar o comando "php artisan migrate"



Foi utilizado o MySQL para o desenvolvimento, porém foi utilizado o eloquent para criação e consultas, sendo assim poderá ser utilizado outro banco de dados.