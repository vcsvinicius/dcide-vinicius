<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('cars.index');
});

Route::group(['prefix' => 'cars', 'as' => 'cars.'], function() {
    Route::get('', ['as' => 'index', 'uses' => 'CarController@index']);
    Route::get('show/{id_car}', ['as' => 'show', 'uses' => 'CarController@show']);
    Route::get('create', ['as' => 'create', 'uses' => 'CarController@create']);
    Route::get('edit/{id_car}', ['as' => 'edit', 'uses' => 'CarController@edit']);
});
