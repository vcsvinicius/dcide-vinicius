<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Routes to CRUD Cars
Route::group(['prefix' => 'cars', 'as' => 'api.cars.', 'namespace' => 'API'], function() {
    Route::post('', ['as' => 'store', 'uses' => 'CarController@store']);
    Route::get('', ['as' => 'get-all', 'uses' => 'CarController@all']);
    Route::get('find', ['as' => 'find', 'uses' => 'CarController@find']);
    Route::get('{id_car}', ['as' => 'get-id', 'uses' => 'CarController@getCar']);
    Route::put('{id_car}', ['as' => 'update', 'uses' => 'CarController@update']);
    Route::delete('{id_car}', ['as' => 'delete', 'uses' => 'CarController@delete']);
});