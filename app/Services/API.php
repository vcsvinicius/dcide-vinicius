<?php

namespace App\Services;

use Curl\Curl;

class API
{
    public function getAllCars()
    {
        $curl = new Curl();
        $cars = $curl->get(route('api.cars.get-all'));

        $curl->close();

        return $cars->response;
    }

    public function getCar($idCard)
    {
        $curl = new Curl();
        $cars = $curl->get(route('api.cars.get-id', ['id_car' => $idCard]));

        $curl->close();

        return $cars->response;
    }
}