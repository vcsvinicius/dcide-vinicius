<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = [
        'car',
        'brand',
        'year',
        'description',
        'sold',
    ];

    public function scopeLikeAllColumns($query, $string)
    {
        $query->where('cars.car', 'like', '%' . $string . '%')
            ->orWhere('cars.brand', 'like', '%' . $string . '%')
            ->orWhere('cars.year', 'like', '%' . $string . '%')
            ->orWhere('cars.description', 'like', '%' . $string . '%')
            ->orWhere('cars.sold', 'like', '%' . $string . '%');
    }
}
