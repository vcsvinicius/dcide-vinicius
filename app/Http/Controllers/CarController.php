<?php

namespace App\Http\Controllers;

use App\Services\API;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * @var API
     */
    private $api;

    /**
     * CarController constructor.
     * @param API $api
     */
    public function __construct(API $api)
    {
        $this->api = $api;
    }

    public function index()
    {
        $cars = json_decode($this->api->getAllCars(), true);
        foreach ($cars as $keyArr => $car) {
            $cars[$keyArr]['sold_label'] = ($cars[$keyArr]['sold'] == 1) ? 'Sim' : 'Não';
            unset($cars[$keyArr]['description']);
            unset($cars[$keyArr]['sold']);
            unset($cars[$keyArr]['created_at']);
            unset($cars[$keyArr]['updated_at']);
        }

        return view('cars.index', compact('cars'));
    }

    public function show($id_car)
    {
        $car = json_decode($this->api->getCar($id_car), true);

        return view('cars.show', compact('id_car', 'car'));
    }

    public function create()
    {
        return view('cars.create');
    }

    public function edit($id_car)
    {
        $car = json_decode($this->api->getCar($id_car), true);

        return view('cars.edit', compact('id_car', 'car'));
    }
}
