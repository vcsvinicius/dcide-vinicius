<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\CarRequest;
use App\Models\Car;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CarController extends Controller
{
    /**
     * @var Car
     */
    private $car;

    /**
     * CarController constructor.
     * @param Car $car
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }

    public function store(Request $request)
    {
        $carRequest = new CarRequest();
        $validator = Validator::make($request->all(), $carRequest->rules(), $carRequest->messages());
        if ($validator->fails())
            return response()->json($validator->errors()->all(), 500);

        $this->car->create($request->all());

        return response()->json('Cadastro realizado com sucesso');
    }

    public function update($car_id, Request $request)
    {
        $carRequest = new CarRequest();
        $validator = Validator::make($request->all(), $carRequest->rules(), $carRequest->messages());
        if ($validator->fails())
            return response()->json($validator->errors()->all(), 500);

        $car = $this->car->find($car_id);
        if ($car) {
            $car->fill($request->all())->save();
            return response()->json('Atualização realizada com sucesso');
        } else
            return response()->json('Registro não encontrado');
    }

    public function delete($car_id)
    {
        $car = $this->car->find($car_id);

        if ($car) {
            $car->delete();
            return response()->json('Exclusão realizada com sucesso');
        } else
            return response()->json('Registro não encontrado');
    }

    public function all()
    {
        return response()->json($this->car->get());
    }

    public function getCar($car_id)
    {
        $car = $this->car->find($car_id);

        if ($car)
            return response()->json($car);
        else
            return response()->json('Registro não encontrado');
    }

    public function find(Request $request)
    {
        $car = $this->car->likeAllColumns($request->get('q'))->get();

        return response()->json($car);
    }
}
