<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'car.required' => 'É necessário informar o Modelo do veículo.',
            'brand.required' => 'É necessário informar a Marca do veículo.',
            'year.required' => 'É necessário informar o Ano do veículo.',
            'year.integer' => 'O campo Ano deverá ter apenas números, exemplo 2019.',
            'year.size' => 'O campo Ano deverá :digits caracteres, exemplo 2019.',
            'description.required' => 'É necessário informar uma Descrição para o veículo.',
            'sold.required' => 'É necessário informar se o veículo já foi Vendido.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car' => 'required',
            'brand' => 'required',
            'year' => 'required|integer|digits:4',
            'description' => 'required',
            'sold' => 'required',
        ];
    }
}
